A Quick Intro to ggplot2
========================================================
author: Fabian Held @FPHeld
date: 16.02.15

What is ggplot2?
========================================================

Implementation of the *Grammar of Graphics* (Wilkinson, 1999) for data visualisation in R

- Strength lies in building complex graphics
- Takes some time to learn, as syntax is different from base graphics  
- Offers ggplot and ~~qplot~~ for plotting
- Creates objects that can then be plotted, altered or saved


Resources
=======================================================

- [www.cookbook-r.com/](http://www.cookbook-r.com/Graphs/)
- [docs.ggplot2.org](http://docs.ggplot2.org/)
- [Second edition of Hadley's book](https://github.com/hadley/ggplot2-book)
- R Graphics Cookbook (Winston Chang)
- First edition of Hadley's book: *ggplot2: Elegant Graphics for Data Analysis* (Use R!) 


Some terminology
========================================================
- ggplot: The main function to specify the dataset and variables
- aes: aesthetics mappings of variables to "visual properties" 
 + e.g. x, y, color, fill, linetype, shape, transparency
- geoms: geometric objects
 + geom_point(), geom_bar(), geom_density(), geom_line(), geom_area(), and many more

[http://docs.ggplot2.org/current/](http://docs.ggplot2.org/current/)



Example
========================================================
*ggplot()* initializes a ggplot object. It can be used to declare the input data frame for a graphic and to specify the set of plot aesthetics intended to be common throughout all subsequent layers unless specifically overridden.


```r
library(ggplot2)

plotobj <- ggplot(data = iris, 
                  aes(x = Sepal.Length, 
                      y = Sepal.Width
                      )
                  )

plotobj <- plotobj + geom_point()
```
*geoms_* determine which visual properties are available in a graph

Example
========================================================

```r
plot(plotobj)
```

![plot of chunk unnamed-chunk-2](Preso_15min_ggplot2.rmd-figure/unnamed-chunk-2-1.png) 

Set size to fixed value
========================================================

```r
plotobj <- plotobj + geom_point(size = 5)
```
![plot of chunk unnamed-chunk-4](Preso_15min_ggplot2.rmd-figure/unnamed-chunk-4-1.png) 

Map a new variable to an aesthetic 
========================================================

```r
plotobj <- plotobj + aes(color = Species)
```
![plot of chunk unnamed-chunk-6](Preso_15min_ggplot2.rmd-figure/unnamed-chunk-6-1.png) 


Map a new variable to an aesthetic in the geom layer
========================================================

```r
plotobj <- plotobj + 
           geom_point(
               aes(shape = Species), 
               colour="black",
               size=3
               )
```
![plot of chunk unnamed-chunk-8](Preso_15min_ggplot2.rmd-figure/unnamed-chunk-8-1.png) 



Repeat with only one layer of points
========================================================

```r
plotobj2 <- ggplot(data = iris, 
                   aes(x = Sepal.Length, 
                       y = Sepal.Width,
                       colour = Species
                       )
                  )
plotobj2 <-plotobj2 + geom_point(
            aes(shape = Species), 
            size=5
            )
```

Example 2
========================================================

```r
plot(plotobj2)
```

![plot of chunk unnamed-chunk-10](Preso_15min_ggplot2.rmd-figure/unnamed-chunk-10-1.png) 

Facetting
========================================================

```r
plot(plotobj2 + facet_grid(Species ~ .))
```

![plot of chunk unnamed-chunk-11](Preso_15min_ggplot2.rmd-figure/unnamed-chunk-11-1.png) 

Colour Scales
========================================================

```r
plot(plotobj2 + scale_colour_manual(
                values=c("red", 
                         "green", 
                         "blue")))
```

![plot of chunk unnamed-chunk-12](Preso_15min_ggplot2.rmd-figure/unnamed-chunk-12-1.png) 


Smoothers, added as another layer
========================================================

```r
plot(plotobj2 + geom_smooth(method="lm"))
```

![plot of chunk unnamed-chunk-13](Preso_15min_ggplot2.rmd-figure/unnamed-chunk-13-1.png) 

And then save your creations
=======================================================
If your plot is on the screen
- ggsave("~/path/to/figure/filename.png")

If your plot is assigned to an object
- ggsave(plotobj2, file = "~/path/to/figure/filename.png")

Specify a size
- ggsave(file = "/path/to/figure/filename.png", width = 6, height =4)

or any format (pdf, png, eps, svg, jpg)
- ggsave(file = "/path/to/figure/filename.eps")
- ggsave(file = "/path/to/figure/filename.jpg")
- ggsave(file = "/path/to/figure/filename.pdf")


Resources
=======================================================

- [www.cookbook-r.com](http://www.cookbook-r.com/Graphs/)
- [docs.ggplot2.org](http://docs.ggplot2.org/)
- [Second edition of Hadley's book](https://github.com/hadley/ggplot2-book)
- R Graphics Cookbook (Winston Chang)
- First edition of Hadley's book: *ggplot2: Elegant Graphics for Data Analysis* (Use R!) 




