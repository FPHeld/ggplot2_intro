A Quick Intro to a Grammar of Graphics
========================================================
author: Fabian Held @FPHeld
date: 16.02.15

Grammar of Graphics?
========================================================

Rules about how to assemble the elements of a plot correctly

- Like grammar of a language specifying how to assemble the elements of a language to correct sentences
- Elements of a language are e.g. subject, object, verb

There are various versions of such a grammar, e.g. Wilkinson (1999), or Wickham (2010)

- Advantages: One conceptual framework to compare plots & one syntax to implement these graphics

Today: "A Layered Grammar of Graphics" aka ggplot2
========================================================
The components of every plot are:

- a dataset and a set of aesthetic mappings (variables to visual properties)

- a coordinate system &  facets


- one or more layers with 
 + one geometric object, 
 + one statistical transformation,
 + one position adjustment (if required)
 
- one scale for each aesthetic mapping



Example
========================================================

```r
library("ggplot2")

ggplot() +
layer( data = diamonds, 
  mapping = aes(x = carat, y = price),
  geom = "point", stat = "identity", 
  position = "identity") +
layer( data = diamonds, 
  mapping = aes(x = carat, y = price),
  geom = "smooth", position = "identity",
  stat = "smooth", method = lm ) +
scale_y_log10() +
scale_x_log10() +
coord_cartesian()
```


========================================================
![plot of chunk unnamed-chunk-2](15min_gg_take2-figure/unnamed-chunk-2-1.png) 


Making use of all the defaults:
========================================================

```r
ggplot(diamonds, aes(carat, price)) +
geom_point() +
stat_smooth(method = lm) +
scale_x_log10() +
scale_y_log10()
```

![plot of chunk unnamed-chunk-3](15min_gg_take2-figure/unnamed-chunk-3-1.png) 

This can be very confusing


Implemented as ggplot2 for R, also available for Python 
========================================================
- Strength lies in building complex graphics
- Takes some time to learn, as syntax is different from base graphics  
- Offers ggplot and ~~qplot~~ for plotting
- Creates objects that can then be plotted, altered or saved


Resources
=======================================================

- [www.cookbook-r.com/](http://www.cookbook-r.com/Graphs/)
- [docs.ggplot2.org](http://docs.ggplot2.org/)
- [Second edition of Hadley's book](https://github.com/hadley/ggplot2-book)
- R Graphics Cookbook (Winston Chang)
- First edition of Hadley's book: *ggplot2: Elegant Graphics for Data Analysis* (Use R!) 




What is available in ggplot2
========================================================

[http://docs.ggplot2.org/current/](http://docs.ggplot2.org/current/)


The power of faceting:
========================================================

```r
ggplot(diamonds, aes(carat, price)) +
geom_point() +
stat_smooth(method = lm, color="black") +
scale_x_log10() +
scale_y_log10() +
facet_wrap(~ cut, ncol=2) +
aes(color=cut)
```

![plot of chunk unnamed-chunk-4](15min_gg_take2-figure/unnamed-chunk-4-1.png) 





Example
========================================================
*ggplot()* initializes a ggplot object. It can be used to declare the input data frame for a graphic and to specify the set of plot aesthetics intended to be common throughout all subsequent layers unless specifically overridden.


```r
library(ggplot2)

plotobj <- ggplot(data = iris, 
                  aes(x = Sepal.Length, 
                      y = Sepal.Width
                      )
                  )

plotobj <- plotobj + geom_point()
```
*geoms_* determine which visual properties are available in a graph

Example
========================================================

```r
plot(plotobj)
```

![plot of chunk unnamed-chunk-6](15min_gg_take2-figure/unnamed-chunk-6-1.png) 

Set size to fixed value
========================================================

```r
plotobj <- plotobj + geom_point(size = 5)
```
![plot of chunk unnamed-chunk-8](15min_gg_take2-figure/unnamed-chunk-8-1.png) 

Map a new variable to an aesthetic 
========================================================

```r
plotobj <- plotobj + aes(color = Species)
```
![plot of chunk unnamed-chunk-10](15min_gg_take2-figure/unnamed-chunk-10-1.png) 


Map a new variable to an aesthetic in the geom layer
========================================================

```r
plotobj <- plotobj + 
           geom_point(
               aes(shape = Species), 
               colour="black",
               size=3
               )
```
![plot of chunk unnamed-chunk-12](15min_gg_take2-figure/unnamed-chunk-12-1.png) 



Repeat with only one layer of points
========================================================

```r
plotobj2 <- ggplot(data = iris, 
                   aes(x = Sepal.Length, 
                       y = Sepal.Width,
                       colour = Species
                       )
                  )
plotobj2 <-plotobj2 + geom_point(
            aes(shape = Species), 
            size=5
            )
```

Example 2
========================================================

```r
plot(plotobj2)
```

![plot of chunk unnamed-chunk-14](15min_gg_take2-figure/unnamed-chunk-14-1.png) 

Facetting
========================================================

```r
plot(plotobj2 + facet_grid(Species ~ .))
```

![plot of chunk unnamed-chunk-15](15min_gg_take2-figure/unnamed-chunk-15-1.png) 

Colour Scales
========================================================

```r
plot(plotobj2 + scale_colour_manual(
                values=c("red", 
                         "green", 
                         "blue")))
```

![plot of chunk unnamed-chunk-16](15min_gg_take2-figure/unnamed-chunk-16-1.png) 


Smoothers, added as another layer
========================================================

```r
plot(plotobj2 + geom_smooth(method="lm"))
```

![plot of chunk unnamed-chunk-17](15min_gg_take2-figure/unnamed-chunk-17-1.png) 

And then save your creations
=======================================================
If your plot is on the screen
- ggsave("~/path/to/figure/filename.png")

If your plot is assigned to an object
- ggsave(plotobj2, file = "~/path/to/figure/filename.png")

Specify a size
- ggsave(file = "/path/to/figure/filename.png", width = 6, height =4)

or any format (pdf, png, eps, svg, jpg)
- ggsave(file = "/path/to/figure/filename.eps")
- ggsave(file = "/path/to/figure/filename.jpg")
- ggsave(file = "/path/to/figure/filename.pdf")


Resources
=======================================================

- [www.cookbook-r.com](http://www.cookbook-r.com/Graphs/)
- [docs.ggplot2.org](http://docs.ggplot2.org/)
- [Second edition of Hadley's book](https://github.com/hadley/ggplot2-book)
- R Graphics Cookbook (Winston Chang)
- First edition of Hadley's book: *ggplot2: Elegant Graphics for Data Analysis* (Use R!) 



